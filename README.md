# grafana-dashboards

- node exporter dashboard: https://grafana.com/grafana/dashboards/11074
- dockerhub rate limit dashboard: https://gitlab.com/gitlab-de/docker-hub-limit-exporter
- kafka exporter: https://grafana.com/grafana/dashboards/7589 
- kong dashboard: https://grafana.com/grafana/dashboards/7424
- cloudflare exporter dashboard: https://grafana.com/grafana/dashboards/13133
- ingress-nginx dashboard: https://github.com/kubernetes/ingress-nginx/blob/main/deploy/grafana/dashboards/nginx.json
- redis dashboard: https://raw.githubusercontent.com/oliver006/redis_exporter/master/contrib/grafana_prometheus_redis_dashboard.json
- kubernetes dashboard: Dùng dashboard export từ rancher (gốc là mixin https://github.com/kubernetes-monitoring/kubernetes-mixin )
- yace aws rds dashboard: Custom from https://promcat.io/apps/aws-rds
- elasticsearch dashboard: Custom from https://github.com/prometheus-community/elasticsearch_exporter/tree/v1.3.0/examples/grafana
- elasticsearch detail: Custom from https://grafana.com/grafana/dashboards/2322/revisions
- mongodb instance overview: https://github.com/percona/grafana-dashboards/blob/main/dashboards/MongoDB/MongoDB_Instances_Overview.json
- gitlab-ci-pipelines: https://grafana.com/grafana/dashboards/13329
- version-checker: https://grafana.com/grafana/dashboards/12833
- kafka-exporter-overviews: https://grafana.com/grafana/dashboards/7589/revisions
- istio dashboards: https://istio.io/latest/docs/ops/integrations/grafana/
- Kubernetes Statistic: Draw by myself
